from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'home.html', {'title':"Hello, I'm Anggito"})

def projects(request):
    return render(request, 'projects.html', {'title':"Projects"})